\chapter{Preliminaries}


\begin{mydef}
Let $A$ be a set. 

We call $R$ an \textit{$n$-ary relation} over 
$A$ if $R\subset A^n$, ie. $R(a_1,\hdots,a_n)$ is true or false
where all $a_i\in A$.

We call $f$ an \textit{$n$-ary function} over $A$ 
if $f:A^n\to A$, ie. $f(a_1,\hdots, a_n)\in A$.
\end{mydef}

\begin{mydef}
\label{model-1}
A \textit{model} is a tuple 
$\mathcal A=(A,R_1,\hdots, R_n, f_1,\hdots,f_m,a_1,\hdots,a_k)$
where $A$ is a non-empty set call the \textit{universe}
of $\mathcal A$ and $R_i$ are relations on $A$ and $f_i$ functions 
on $A$ and $a_i$ constants from $A$.
\end{mydef}

\begin{ex}
The structure of the naturals $\mathbb N=(\mathbb N, <,+, \times,0)$ 
is a model. 
Groups, rings, etc. are also models.
\end{ex}

\begin{mydef}
The \textit{logical symbols} of a \textit{first-order language} 
(and, indeed, first-order logic in general)
are 
\begin{enumerate}
\item countably many variables,
\item the equality relation $=$,
\item the symbols for connectives,
$\lnot$, $\land$, $\lor$, $\rightarrow$, $\leftrightarrow$,
and quantifiers, $\forall$, $\exists$,
\item symbols to ease readibility: eg. parentheses, comma, etc.
\end{enumerate}
\end{mydef}
\begin{mydef}
The set $L$ of  \textit{non-logical symbols} is called the 
\textit{vocabulary} of a first-order language and
includes:
\begin{enumerate}
\item relation symbols,
\item function symbols,
\item constant symbols or (individual) constants.
\end{enumerate}
\end{mydef}

\begin{mydef}
An \textit{expression} from a vocabular $L$ is a finite 
sequence of $L$-symbols.
\end{mydef}

\begin{mydef}
We define a class of expressions called \textit{$L$-terms} recursively:
\begin{enumerate}
\item variables and constants of $L$ are $L$-terms,
\item If $f\in L$ is an $n$-ary function symbol and $t_i$ are $L$-terms, 
then so is $f(t_1,\hdots, t_n)$.
\end{enumerate}
\end{mydef}

\begin{pro}[Term Induction]
Suppose $X$ is a set of $L$-terms containing:
\begin{enumerate}
\item all variables and individual constant from $L$,
\item contains a term $f(t_1,\hdots,t_n)$ whenever
$f$ is an $n$-ary $L$-function symbol and $t_i\in X$.
\end{enumerate}
Then $X$ contains all $L$-terms.
\end{pro}

\begin{mydef}
The \textit{subterms} of a term $t$, written $Subt(t)$, are defined as follows:
\begin{enumerate}
\item if $t$ is a variable or a constant symbol, then $Subt(t)=\{t\}$,
\item if $t=f(t_1,\hdots,t_n)$, then 
$Subt(t)=\{t\}\cup Subt(t_1)\cup\cdots\cup Subt(t_n)$.
\end{enumerate}
\end{mydef}

\begin{mydef}
We define a class of expressions called \textit{(first-order) $L$-formulas} 
recursively:
\begin{enumerate}
\item the expression $s=t$ where $s$, $t$ are $L$-terms is an $L$-formula,
\item if $t_i$ are $L$-terms and $r$ an $n$-ary relation symbol, then
$r(t_1,\hdots, t_n)$ is an $L$-formula,
\item if $\varphi$, $\psi$ are $L$-formulas, then 
the forms $\lnot\varphi$, $(\varphi\land\psi)$, $(\varphi\lor\psi)$,
$(\varphi\rightarrow\psi)$, $(\varphi\leftrightarrow\psi)$, $\forall x\varphi$,
$\exists x\varphi$ are all $L$-formulas.
\end{enumerate}
We call formulas of the form $s=t$ \textit{equalities}. 
Equalities and formulas of the form 
$r(t_1,\hdots,t_n)$ are \textit{atoms}.
We call $\lnot\varphi$ the \textit{negation} of $\varphi$
and $(\varphi\land\psi)$, $(\varphi\lor\psi)$, 
$(\varphi\rightarrow\psi)$, $(\varphi\leftrightarrow\psi)$
the \textit{conjunction}, \textit{disjunction}, \textit{implication}, 
\textit{equivalence}, respectively, of $\varphi$ and $\psi$. 
The formulas $\forall x\varphi$ and $\exists x\varphi$ are the 
\textit{universal} and \textit{existential quantifications} of 
$\varphi$ with respect to the variable $x$.
\end{mydef}

\begin{pro}[Formula Induction]
If a set of $L$-formulas contains $L$-atoms and 
is closed under logical operations, then it contains
all $L$-formulas.
\end{pro}

\begin{mydef}
The set $Subf(\varphi)$ denotes the
 \textit{subformulas} of a formula $\varphi$ which is defined 
as follows:
\begin{enumerate}
\item if $\varphi$ is an atom, then $Subf(\varphi)=\{\varphi\}$,
\item $Subf(\lnot\varphi)=\{\lnot\varphi\}\cup Subf(\varphi)$,
\item $Subf(\forall x\varphi)=\{\forall x\varphi\}\cup Subf(\varphi)$ 
and similarly for existential quantification,
\item $Subf(\varphi\land\psi)=\{\varphi\land\psi\}\cup Subf(\varphi)
\cup Subf(\psi)$ and similarly for disjunctions, implications, and equivalences.
\end{enumerate}
\end{mydef}

\begin{mydef}
The \textit{scope} of a logical constant in a formula consists 
of all the subformulas to which the constant is applied.
\end{mydef}

\begin{mydef}
Variables in the scope of a quantifier are \textit{bounded} by 
the first quantifier for which the subformulas contain said variable,
eg. the scope of $\forall x$ in $\forall x(r_1(x)\land \exists x r_2(x))$
binds the $x$ in $r_1(x)$ but not the $x$ in $r_2(x)$. 
The $x$ in $r_2(x)$ is bounded by $\exists x$.
An unbounded variable is \textit{free}.
\end{mydef}


\begin{mydef}
An $L$-formula with no free variables is called an \textit{$L$-sentence}.
\end{mydef}

\begin{mydef}
Fix a vocabulary $L$. 
An $L$-model $\mathcal A$ is a couple consisting of a non-empty 
set $A$, the \textit{universe} of $\mathcal A$, and 
an operation $\sigma\to\sigma^\mathcal A$ 
on the non-logical symbols $\sigma$ of $L$ satisfying:
\begin{enumerate}
\item if $r\in L$ is an $n$-ary relation symbol, then $r^\mathcal A$
is an $n$-ary relation over $A$, 
\item if $f\in L$ is an $n$-ary function symbol, then $f^\mathcal A$
is an $n$-ary function over $A$,
\item if $c\in L$ is a constant symbol, then $c^\mathcal A\in A$.
\end{enumerate}
We call $\sigma^\mathcal A$ the \textit{interpretation} or 
\textit{meaning} of $\sigma$ in $\mathcal A$
and, conversely, $\sigma$ is the \textit{name} of $\sigma^\mathcal A$.
\end{mydef}

\begin{rmk}
We may represent a model $\mathcal A$ over a universe $A$ with 
vocabulary $L=\{r_1,\cdots,r_n, f_1, \cdots,f_m, c_1,\hdots, c_k\}$
in the form 
$\mathcal A =(A,r_1^\mathcal A,\cdots,r_n^\mathcal A, 
f_1^\mathcal A, \cdots,f_m^\mathcal A, c_1^\mathcal A,\hdots, c_k^\mathcal A)$.
This agrees with our definition in \cref{model-1}.
\end{rmk}

\begin{mydef}
The \textit{cardinality} of a model is the cardinality of its universe.
\end{mydef}

\begin{mydef}
We say a model is a \textit{purely relational} if its vocabulary 
contains relation symbols only.
\end{mydef}

\begin{mydef}
Let $A$ be the universe of a model $\mathcal A$. 
An \textit{$\mathcal A$-assignment} is a function from 
the set of variables into $A$.
\end{mydef}

\begin{mydef}
Let $L$ be a vocabulary, $\mathcal A$ an $L$-model, $\alpha$ an 
$\mathcal A$-assignment.
Let $t$ be a term. We define an element $t^\mathcal A[\alpha]\in A$ called
the \textit{value} of $t$
recursively:
\begin{enumerate}
\item  if $t$ is a variable $x$, then $t^\mathcal A[\alpha]=\alpha(x)$,
\item  if $t$ is a constant symbol $c$, then $t^\mathcal A[\alpha]=c^\mathcal A$,
\item if $t=f(t_1,\hdots, t_n)$, then 
$t^\mathcal A[\alpha]=
f^\mathcal A(t_1^\mathcal A[\alpha],\hdots,t_n^\mathcal A[\alpha])$.
\end{enumerate}
\end{mydef}

\begin{mydef}[Tarski's defnition of satisfaction]
Let $L$, $\mathcal A$, $\alpha$ be as before.
Let $\varphi$ be a formula. 
Define $\mathcal A\models \varphi[a]$ by the following rules:
\begin{align*}
\mathcal A\models(s=t)[\alpha]&\iff s^\mathcal A[\alpha]=t^\mathcal A[\alpha], \\
\mathcal A\models r(t_1,\hdots, t_n)[\alpha] 
  &\iff r^\mathcal A(t_1^\mathcal A[\alpha],\hdots,t_n^\mathcal A[\alpha]), \\
\mathcal A\models\lnot\varphi[\alpha]&\iff\mathcal A\not\models\varphi[\alpha],\\
\mathcal A\models(\varphi\lnot\psi)[\alpha]
  &\iff\mathcal A\models\varphi[\alpha]\text{ and }\mathcal A\models\psi[\alpha]
   & \text{(and similarly for other connectives)}, \\
\mathcal A\models\exists x\varphi[\alpha]
&\iff\exists a\in A, \mathcal A\models\varphi[\alpha_a^x]
   & \text{(and similarly for $\forall$)}
\end{align*}
where $\alpha_a^x$ denotes the modification of $\alpha$ that sends the variable
$x$ to $a$ but is otherwise the same as $\alpha$.
We say \textit{$\varphi$ is satisfied by $\alpha$ in $\mathcal A$}.
\end{mydef}






































